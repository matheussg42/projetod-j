<?php
class FuncionarioController extends BaseController{
	

	public function __construct()
	{
		$this->beforeFilter('auth');
	}
	
	private $rules = array('nome' => 'required|min:3', 'email' => 'required|email', 'setor' => 'required', 'cargo' => 'required',  'imagem' => 'required');
	
	public function getIndex(){
		$func = DB::table('func')->get();
		$title = 'Lista de Funcionários';
		
		return View::make('cadfuncionario.lista', compact('func', 'title'));
	}
	
	public function getCadastrar(){
		$tituloPagina = 'Cadastrar Funcionário';
		
		return View::make('cadfuncionario.new-edit', compact('tituloPagina'));
		
	}
	
	public function postAdicionar(){
		$dadosFormulario = Input::all();
		$validator = Validator::make($dadosFormulario, $this->rules);
		
		if($validator->fails() ){
			return Redirect::to('painel/cadastrar')
					->withErrors($validator)
					->withInput();			
		}
		
		$destinationPath = 'uploads/';
		$filename = $dadosFormulario["imagem"]->getClientOriginalName();
		$dadosFormulario["imagem"]->move($destinationPath, $filename);
		$dadosFormulario["imagem"] = $filename;

		$funcionarios = new FuncionarioDados($dadosFormulario);
		$funcionarios->save();
		
		return Redirect::to('painel');
	}
	
	public function getEditar($idfuncionario){
		$func = FuncionarioDados::find($idfuncionario);
		$tituloPagina = 'Editar Funcionário';
		
		return View::make('cadfuncionario.new-edit', compact('func', 'tituloPagina'));
	}
	
	public function postEditar($idfuncionario){
		$dadosFormulario = Input::all();
		
		$validator = Validator::make($dadosFormulario, $this->rules);
		
		if($validator->fails() ){
			return Redirect::to("painel/editar/{$idfuncionario}")
			->withErrors($validator)
			->withInput();
		}
		
		$func = FuncionarioDados::find($idfuncionario);
		if(!isset($dadosFormulario["imagem"]) || $dadosFormulario["imagem"] == ""){
			$dadosFormulario["imagem"] = $func["imagem"];
		}else{
			$destinationPath = 'uploads/';
			$filename = $dadosFormulario["imagem"]->getClientOriginalName();
			$dadosFormulario["imagem"]->move($destinationPath, $filename);
			$dadosFormulario["imagem"] = $filename;
		}
		
		$func ->__construct($dadosFormulario);
		$func->save();
		
		return Redirect::to('painel');
	}
	
	public function getDeletar($idfuncionario){
		$funcionarios = FuncionarioDados::find($idfuncionario);
		
		
		$funcionarios ->delete();
		unlink('uploads/'.$funcionarios["imagem"]);
		return Redirect::to('painel');
	}
	
	public function MissingMethod($params = array()){
		return 'Volte para uma pagina válida!<a href="'.URL::to('painel').'">Clique aqui!</a>';
	}
}