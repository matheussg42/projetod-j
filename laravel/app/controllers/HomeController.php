<?php

class HomeController extends BaseController {

	private $rules = array('nome' => 'required|min:3', 'email' => 'required|email', 'senha' => 'required');


	public function getIndex(){

		return View::make('home.login');
	}

	public function getLogin(){

		return View::make('home.login');
	}

	public function postLogin(){
		$rules = array(
				'email'            => 'required|email',
				'password'         => 'required',);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			$messages = $validator->messages();

			return Redirect::to('home/login')
			->withErrors($validator);

		} else {

			$userdata = array(
					'email' => Input::get('email'),
					'password' => Input::get('password'));

			if(Auth::attempt($userdata)){
				return Redirect::to('painel');
			}else{
				return Redirect::to('home');
			}
		}
	}


	public function getSignup()	{

		return View::make('home.cadastroUser');
	}

	public function postSignup(){
		$rules = array(
				'username'         => 'required',
				'email'            => 'required|email|unique:users',
				'password'         => 'required',
				'password_confirm' => 'required|same:password');

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			$messages = $validator->messages();

			return Redirect::to('home/signup')
			->withErrors($validator);

		} else {

				
			$userdata = array(
					'username' =>Input::get('username'),
					'email' => Input::get('email'),
					'password' => Hash::make(Input::get('password')) );
				
			$user = new User($userdata);
			$user ->save();

			return Redirect::to('home/login');
		}
	}

	public function getDashboard(){
		echo 'Bem vindo, Você esta logado! Faça o <a href="'.URL::to('logout').'">logout?</a>';
	}

	public function getLogout(){

		Auth::logout();
		Session::flush();
		return Redirect::to('login');
	}

	public function MissingMethod($params = array()){
		return 'Volte para uma pagina válida!<a href="'.URL::to('home/login').'">Clique aqui!</a>';
	}
}
