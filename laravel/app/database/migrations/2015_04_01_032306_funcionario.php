<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Funcionario extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('func', function($table){
			$table->increments('idfuncionario');
			$table->string('nome', 45);
			$table->string('email', 45);
			$table->string('setor', 45);
			$table->string('cargo', 45);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
}