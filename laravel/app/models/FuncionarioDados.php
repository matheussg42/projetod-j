<?php

class FuncionarioDados extends Eloquent{
	
	protected $table = 'func';
	
	protected $primaryKey ='idfuncionario';
	
	public $timestamps = false;
	
	protected $guarded = array('idfuncionario');
}