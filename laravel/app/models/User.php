<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	protected $primaryKey ='id';
	
	public $timestamps = false;
	
	protected $guarded = array('id');
	
	
	protected $hidden = array('password', 'remember_token');
	protected $fillable = array('username', 'email', 'password');

}
