<?php

Route::controller('painel', 'FuncionarioController');
Route::controller('home', 'HomeController');

Route::get('/', function(){

	return Redirect::to('home');
});

Route::get('logout', function(){
	
	Auth::logout();
    Session::flush();
	return Redirect::to('home/login');
});