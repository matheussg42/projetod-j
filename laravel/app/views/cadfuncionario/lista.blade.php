@extends('template.template') @section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<h1 class="page-header">Cadastro de Funcionário</h1>
			
			
			<a href="{{url('painel/cadastrar')}}" title="Cadastrar">
				<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
			</a>
			
			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>Nome</th>
							<th>E-Mail</th>
							<th>Setor</th>
							<th>Cargo</th>
							<th>Foto</th>
							<th>Ações</th>
						</tr>
					</thead>
					<tbody>
						@forelse($func as $func)
						<tr>
							<td>{{$func->idfuncionario}}</td>
							<td>{{{$func->nome}}}</td>
							<td>{{{$func->email}}}</td>
							<td>{{{$func->setor}}}</td>
							<td>{{{$func->cargo}}}</td>
							<td><img src="uploads/{{{$func->imagem}}}" width="40px" /></td>
							<td>
							<a href='{{url("painel/editar/{$func->idfuncionario}")}}' title="Editar">
								<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
							</a>
							&nbsp;
							/
							<a href='{{url("painel/deletar/{$func->idfuncionario}")}}' onclick="return confirm('Deseja mesmo deletar esses dados?')" title="excluir">
								<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
							</a>
							</td>
						</tr>
						@empty
						<tr>
							<td colspan="6">Nenhum dado. {{HTML::link("painel/cadastrar",
								'Cadastrar aqui! :)')}}</td>
						</tr>

						@endforelse
					</tbody>
				</table>
				</p>
			</div>
		</div>
	</div>
</div>

@stop
