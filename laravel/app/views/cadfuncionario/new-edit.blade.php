@extends('template.template') @section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-4 col-md-10 col-md-offset-4 main">
			<div class="col-sm-8 ">
					<h2 class="form-signin-heading margin-bottom-30">{{$tituloPagina or 'Gestão de Funcionário'}}</h2>

					@if( count ($errors) > 0)
						<div class="alert alert-danger">
							@foreach($errors->all() as $message)
								<p>{{$message}}</p>
							@endforeach
						</div>
					@endif
	
					@if( isset($func->idfuncionario) )
						{{Form::open(array('url' => "painel/editar/{$func->idfuncionario}", 'files' => true))}}
					@else	
						{{Form::open(array('url' => 'painel/adicionar', 'files' => true))}}
					@endif
					
					<div class="row margin-bottom-10">
						<div class="col-sm-2">
							<label for="inputNome" >Nome:</label>
						</div>
						<div class="col-sm-6 ">	
							{{Form::text('nome', isset($func->nome) ? $func ->nome: '', array('placeholder' => 'Digite seu nome:', 'class'=>'form-control'))}}<p>
						</div>
					</div>	
					<div class="row margin-bottom-10">
						<div class="col-sm-2">
							<label for="inputNome" >E-Mail:</label>
						</div>
						<div class="col-sm-6 ">	
							{{Form::text('email', isset($func->email) ? $func ->email: '', array('placeholder' => 'Digite seu email:', 'class'=>'form-control '))}}<p>
						</div>
					</div>
					<div class="row margin-bottom-10">
						<div class="col-sm-2">
							<label for="inputNome" >Setor:</label>
						</div>
						<div class="col-sm-6 ">
							{{Form::text('setor', isset($func->setor) ? $func ->setor: '', array('placeholder' => 'Digite seu setor:', 'class'=>'form-control'))}}<p>
						</div>
					</div>
						
						<div class="row margin-bottom-10">
						<div class="col-sm-2">
							<label for="inputNome" >Cargo:</label>
						</div>
						<div class="col-sm-6 ">
							{{Form::text('cargo', isset($func->cargo) ? $func ->cargo: '', array('placeholder' => 'Digite seu cargo:', 'class'=>'form-control'))}}<p>
						</div>
					</div>
					
					<div class="row margin-bottom-10">
						@if( isset($func->imagem) )
						<div>
							<img src="{{url("uploads/{$func->imagem}")}}" height="100px"/>
						</div>
						@endif
					</div>
					
					<p>
				        {{ Form::label('imagem', 'Avatar') }}
				        {{ Form::file('imagem') }}
				    </p>
									
					
					@if( isset($func->idfuncionario) )	
						<div class="col-sm-3 ">
							{{Form::submit('Salvar', array('class' => 'btn btn-lg btn-primary btn-block'))}}
						</div>
					@else
						<div class="col-sm-3 ">
							{{Form::submit('Enviar', array('class' => 'btn btn-lg btn-primary btn-block'))}}
						</div>
					@endif
						<div class="col-sm-3 col-md-offset-2">
							<a href='{{url("painel")}}' onclick="return confirm('Deseja mesmo cancelar o Formulário?')" title="Cancelar">
								<span class="btn btn-lg btn-block btn-warning" aria-hidden="true">Cancelar</span>
							</a>
						</div>
					{{Form::close()}}
			
			</div>
		</div>
	</div>
</div>

@stop