@extends('template.templateLogin')

@section('content')
<div id="loginModal" class="modal show" tabindex="-1" role="dialog"
	aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				
				<h1 class="text-center">Cadastro De Usuários</h1>
			</div>
			<div class="modal-body">
				
				@if( count ($errors) > 0)
						<div class="alert alert-danger">
							@foreach($errors->all() as $message)
								<p>{{$message}}</p>
							@endforeach
						</div>
					@endif
				
				
					{{Form::open(array('url' => 'home/signup'))}}

							<label for="inputLogin">Nome:</label>
							{{Form::text('username', '', array('placeholder' => 'Digite seu Nome:', 'class' => 'form-control'))}}
							
							<label for="inputLogin">E-Mail:</label>
							{{Form::text('email', '',array('placeholder' => 'Digite seu e-mail:', 'class' => 'form-control'))}}
							
							<label for="inputNome">Senha:</label>
							{{Form::password('password', array('class' => 'form-control'))}} <br>
							
							<label for="inputNome">Confime a Senha:</label>
							{{Form::password('password_confirm', array('class' => 'form-control'))}} <br>
							
							{{Form::submit('Cadastrar', array('class' => 'btn btn-lg btn-primary '))}}
						
						<a href='{{url("home/login")}}' onclick="return confirm('Deseja mesmo cancelar o Formulário?')" title="Cancelar">
							<span class="btn btn-lg btn-warning pull-right" aria-hidden="true">Cancelar</span>
						</a>
					</div>
				{{Form::close()}}
			</div>
		</div>
	</div>
</div>
@stop