﻿@extends('template.templateLogin') @section('content')
<div id="loginModal" class="modal show" tabindex="-1" role="dialog"
	aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				
				<h1 class="text-center">Login</h1>
			</div>
			<div class="modal-body">
					@if( count ($errors) > 0)
						<div class="alert alert-danger">
							@foreach($errors->all() as $message)
								<p>{{$message}}</p>
							@endforeach
						</div>
					@endif
					
						{{ Form::open(array('url' => 'home/login')) }}

						
						{{Form::label('email', 'E-mail:') }}
						{{Form::text('email', '', array('class' => 'form-control'))}}
						
						{{Form::label('password', 'Password') }}
						{{Form::password('password', array('class' => 'form-control'))}} <br>
						
						{{Form::submit('Login', array('class' => 'btn btn-primary')) }}
						
							<a href='{{url("home/signup")}}' title="Cadastro de Novos Usuários">
								Não tem um usuário? Cadastre um aqui!
							</a>
						
						{{Form::close()}}
					</div>
				</div>
			</div>
		</div>
@stop