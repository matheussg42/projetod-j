<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>{{$title or 'Cadastro de Funcionários'}}</title>
	
	{{HTML::style('assests/css/bootstrap.css')}}
	{{HTML::style('assests/css/bootstrap.min.css')}}
	{{HTML::style('assests/css/custom.css')}}
	
</head>
<body id="page-top" class="index">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                {{HTML::link("painel", 'CadFuncionário', array('class'=>'navbar-brand'))}}
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        {{HTML::link("painel", 'Funcionários')}}
                    </li>
                    <li class="page-scroll">
                        {{HTML::link("painel/cadastrar", 'Cadastrar')}}
                    </li>
                    <li class="page-scroll">
                        {{HTML::link("logout", 'Logout')}}
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
	
	<div style="margin-bottom: 250px; margin-top: 250px" class="content">
		@yield('content')
	</div>
	
	<footer class="text-center">
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; <a href="http://br.linkedin.com/pub/matheus-santos/96/95/125">Matheus Santos</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    
    {{ HTML::script('js/jquery.min.js') }}
	{{ HTML::script('js/bootstrap.js') }}
</body>
</html>