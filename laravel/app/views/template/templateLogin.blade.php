<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>{{$title or 'Cadastro de Funcionários'}}</title>
	
	{{HTML::style('assests/css/bootstrap.css')}}
	{{HTML::style('assests/css/bootstrap.min.css')}}
	{{HTML::style('assests/css/custom.css')}}
	
</head>
<body id="page-top" class="index">
	
	<div style="margin-bottom: 250px; margin-top: 250px" class="content">
		@yield('content')
	</div>
	
    {{ HTML::script('js/jquery.min.js') }}
	{{ HTML::script('js/bootstrap.js') }}
</body>
</html>